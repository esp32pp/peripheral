#include <stdio.h>
#include <iostream>

#include "time_macros.hpp"
#include "i2c.hpp"

void test_function();


// ############################################################################################
// 	Utilities
// ############################################################################################


void print_header()
{
	std::cout << "Compiler Version: " << __VERSION__ << std::endl;
	std::cout << " ESP-IDF Version: " << esp_get_idf_version() << std::endl;
	std::cout << std::endl;
	
}

// ############################################################################################
// 	MAIN
// ############################################################################################
extern "C" void app_main()
{
	print_header();

	printf("[%015lld]\tWelcome\n", NOW_MS());

	peripheral::i2c::i2c_bus<I2C_NUM_0, GPIO_NUM_19, GPIO_NUM_18>::instance()->discover();
	printf("\n");
	
	peripheral::i2c::bus_client<I2C_NUM_0, GPIO_NUM_19, GPIO_NUM_18> bme280(3400000);
	peripheral::i2c::bus_client<I2C_NUM_0, GPIO_NUM_19, GPIO_NUM_18> bmx055(400000);

	bme280.open();
	bmx055.open();

	peripheral::i2c::packet cmd(0,1);
	cmd.reg_addr = 0xD0;
	cmd.node_addr = 0x77;
	cmd.mode = peripheral::TRANSACTION::READ;
	bme280.push(cmd);
	printf("    BME280 ChipID: %#02x\n", cmd[0]);


	cmd.reg_addr = 0x00;
	cmd.node_addr = 0x19;
	cmd.mode = peripheral::TRANSACTION::READ;
	bmx055.push(cmd);
	printf("BMX055 ACC ChipID: %#02x\n", cmd[0]);

	cmd.reg_addr = 0x00;
	cmd.node_addr = 0x69;
	cmd.mode = peripheral::TRANSACTION::READ;
	bmx055.push(cmd);
	printf("BMX055 GYR ChipID: %#02x\n", cmd[0]);

	cmd.reg_addr = 0x40;
	cmd.node_addr = 0x13;
	cmd.mode = peripheral::TRANSACTION::READ;
	bmx055.push(cmd);
	printf("BMX055 MAG ChipID: %#02x\n", cmd[0]);

	printf("[%015lld]\tSetup complete\n",  NOW_MS());
}