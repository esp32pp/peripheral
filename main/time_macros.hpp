#pragma once

#include <chrono>

// #include <time.h>
// #include <sys/time.h>

// ***********************
// Time Macros
#define NS_SINCE_EPOCH(t) std::chrono::duration_cast<std::chrono::nanoseconds>((t).time_since_epoch()).count()
#define US_SINCE_EPOCH(t) std::chrono::duration_cast<std::chrono::microseconds>((t).time_since_epoch()).count()
#define MS_SINCE_EPOCH(t) std::chrono::duration_cast<std::chrono::milliseconds>((t).time_since_epoch()).count()
#define S_SINCE_EPOCH(t) std::chrono::duration_cast<std::chrono::seconds>((t).time_since_epoch()).count()

#define NOW() 		std::chrono::system_clock::now()
#define NOW_S()		S_SINCE_EPOCH(NOW()) 		
#define NOW_MS()	MS_SINCE_EPOCH(NOW()) 		
#define NOW_US()	US_SINCE_EPOCH(NOW()) 		
#define NOW_NS()	NS_SINCE_EPOCH(NOW()) 		

typedef std::chrono::system_clock::time_point time_point_t;
// ***********************