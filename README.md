# C++ peripheral driver for the ESP32

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
* [esp-idf](https://esp-idf.readthedocs.io/en/latest/get-started/index.html) - Getting started

### Installing
* Clone this repository
* run `make menuconfig` in the root directory and adapt the serial port settings to your system

## Running the tests

## Deployment
* Run `make flash` while the EPS32 is conencted to your machine

## Built With

* [esp-idf v3.1-dev-145-gc401a74](https://esp-idf.readthedocs.io/en/latest/) - Compiling and flashing toolchain
* [Doxygen](http://www.stack.nl/~dimitri/doxygen/) - Documentation

## Dependencies

## Documentation
The full documentation is located in the respective [wiki](https://gitlab.lrz.de/esp32pp/peripheral/wikis/home)

## License
This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details