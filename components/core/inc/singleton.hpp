#pragma once

// class N
// {
// public:
// 	static N& instance()
// 	{
// 	static N _instance;
// 	return _instance;
// 	}
// 	~N() {}
// 	void xyz();
// private:
// 	N() {}           // verhindert, dass ein Objekt von außerhalb von N erzeugt wird. protected, wenn man von der Klasse noch erben möchte
// public:
// 	N( const N& ) = delete; /* verhindert, dass eine weitere Instanz via Kopier-Konstruktor erstellt werden kann */
// 	N & operator = (const N &) = delete; //Verhindert weitere Instanz durch Kopie
// };

template <typename C>
class Singleton
{
public:
	static C* instance ()
	{
		if (!_instance)
			_instance = new C ();
		return _instance;
	}
	virtual ~Singleton ()
	{ _instance = 0; }
private:
	static C* _instance;
protected:
	Singleton () { }
};
template <typename C> C* Singleton <C>::_instance = nullptr;