#pragma once

#include <stdio.h>
#include <cstring>
#include <exception>
#include <string>
#include <vector>

#include "driver/i2c.h"
#include "driver/gpio.h"
#include "singleton.hpp"

namespace peripheral
{
	class peripheral_error : public std::logic_error
	{
	public:
		peripheral_error() : std::logic_error("Generic peripheral error") {};		
		peripheral_error(const std::string& what) : std::logic_error(what) {};		
	};

	enum class TRANSACTION : uint8_t
	{
		WRITE 	= 0,
		READ 	= 1
	};

	namespace i2c
	{
		static SemaphoreHandle_t i2c_port_mux[2] = {nullptr, nullptr};
		const static uint8_t PUSH_RETRY_ATTEMPTS = 3;

		enum class ADDR_FORMAT : uint8_t
		{
			NORMAL7	= 0,	// 7-bit address
			LONG10 	= 1		// 10-bit address
		};

		enum class BYTE_ORDER : uint8_t
		{
			LSB_FIRST = 0,
			MSB_FIRST = 1
		};

		enum class I2C_ISR_SOURCE : uint8_t
		{
			GENERIC 				= 0,
			TX_SEND_EMPTY_INT 		= 1, // Triggered when the I2C has sent nonfifo_tx_thres bytes of data.
			RX_REC_FULL_INT 		= 2, // Triggered when the I2C has received nonfifo_rx_thres bytes of data.
			ACK_ERR_INT 			= 3, // Triggered when the I2C Master receives an ACK that is not as expected, or when the I2C Slave receives an ACK whose value is 1.
			TRANS_START_INT 		= 4, // Triggered when the I2C sends the START bit.
			TIME_OUT_INT 			= 5, // Triggered when the SCL stays high or low for more than I2C_TIME_OUT clocks.
			TRANS_COMPLETE_INT 		= 6, // Triggered when the I2C detects a STOP bit.
			MASTER_TRAN_COMP_INT 	= 7, // Triggered when the I2C Master sends or receives a byte.
			ARBITRATION_LOST_INT 	= 8, // Triggered when the I2C Master’s SCL is high, while the output value and input value of the SDA do not match.
			SLAVE_TRAN_COMP_INT 	= 9, // Triggered when the I2C Slave detects a STOP bit.
			END_DETECT_INT 			= 10 // Triggered when the I2C deals with the END command.
		};

		class packet : public std::vector<uint8_t>
		{
		public:
			packet() : packet(0, 0) {};		
			packet(uint16_t tx_length, uint16_t rx_length) : std::vector<uint8_t>(tx_length, 0)
			{
				mode 		= TRANSACTION::READ;
				addr_format = ADDR_FORMAT::NORMAL7;
				
				node_addr 	= 0x00;
				reg_addr 	= 0x00;
				
				wr_delay_ms = 0;

				setBufferLength(tx_length, rx_length);
			}

			void setBufferLength(uint16_t tx_length, uint16_t rx_length)
			{
				rx_len = rx_length;
				tx_len = tx_length;
				std::vector<uint8_t>::reserve(std::max(tx_length, rx_length));

				if(std::vector<uint8_t>::size() < tx_length)
					std::vector<uint8_t>::insert(std::vector<uint8_t>::end(), std::vector<uint8_t>::size() - tx_length, 0);
			}

			inline uint16_t getTxBufferLen() const { return tx_len; };
			inline uint16_t getRxBufferLen() const { return rx_len; };

			TRANSACTION mode;
			ADDR_FORMAT	addr_format;
			uint16_t 	node_addr;
			uint16_t 	reg_addr;
			uint16_t 	wr_delay_ms;

		private:
			uint16_t 	tx_len;
			uint16_t 	rx_len;
		};

		template<i2c_port_t PORT, gpio_num_t SDA_PIN, gpio_num_t SCL_PIN>
		class i2c_bus : public Singleton< i2c_bus<PORT, SDA_PIN, SCL_PIN> >
		{
			friend class Singleton< i2c_bus<PORT, SDA_PIN, SCL_PIN> >; 
		protected:
			i2c_bus()
			{
				if(isPort1() && (i2c_port_mux[0] == nullptr))
					i2c_port_mux[0] = xSemaphoreCreateMutex();
				else if (!isPort1() && (i2c_port_mux[1] == nullptr))
					i2c_port_mux[1] = xSemaphoreCreateMutex();
				else
					throw peripheral_error("Port already instanciated");
			};

		public:
			~i2c_bus() { close(); };
			
			inline bool isOK() const { return _isOK; };

			inline void open_master(uint32_t clk_speed_Hz)
			{ i2c_bus::open(I2C_MODE_MASTER, 0, 0, 0, clk_speed_Hz, false, 0); };

			void open(i2c_mode_t mode, 
						size_t slv_rx_buf_len, 
						size_t slv_tx_buf_len, 
						int intr_alloc_flags, 
						uint32_t clk_speed_Hz,
						bool slave_10bit_addr,
						uint16_t slave_addr)
			{
				if(isOK())
					close();

				i2c_config_t cfg;
				cfg.mode 			= mode;
				cfg.sda_io_num 		= SDA_PIN;
				cfg.sda_pullup_en 	= GPIO_PULLUP_ENABLE;
				cfg.scl_io_num 		= SCL_PIN;
				cfg.scl_pullup_en 	= GPIO_PULLUP_ENABLE;
				if(isMaster())
				{
					if(clk_speed_Hz <= 1000000)	// docu defines maximum 1MHz
						cfg.master.clk_speed 	= clk_speed_Hz;
					else
						cfg.master.clk_speed 	= 1000000;
				}
				else
				{
					cfg.slave.addr_10bit_en 	= (uint8_t)slave_10bit_addr;
					cfg.slave.slave_addr 		= slave_addr;
				}
				_err = i2c_param_config(PORT, &cfg);
				if(_err != ESP_OK)
				{
					close();
					throw peripheral_error("Invalid arguments in I2C configuration");
				}
				_clk_speed_Hz 		= clk_speed_Hz;
				_slave_10bit_addr	= slave_10bit_addr;
				_slave_addr 		= slave_addr;

				_err = i2c_driver_install(PORT, mode, slv_rx_buf_len, slv_tx_buf_len, intr_alloc_flags);
				if(_err != ESP_OK)
				{
					if(_err == ESP_ERR_INVALID_ARG)
						throw peripheral_error("Invalid arguments in I2C configuration");
					else if (_err == ESP_FAIL)
						throw peripheral_error("Failed to install driver");
						
				}
				_mode 				= mode;
				_slv_rx_buf_len 	= slv_rx_buf_len;
				_slv_tx_buf_len 	= slv_tx_buf_len; 
				_intr_alloc_flags 	= intr_alloc_flags; 

				_isOK = true;
			};

			void close()
			{
				lock_mutex();
				if(isOK())	// prevent closing an non-open port
				{	
					_isOK = false;
					_err = i2c_reset_tx_fifo(PORT);
					_err = i2c_reset_rx_fifo(PORT);
					_err = i2c_driver_delete(PORT);
				}

				unlock_mutex();


				if(_err == ESP_ERR_INVALID_ARG)
						throw peripheral_error("Invalid arguments for closing I2C port");
			};

			void push(i2c::packet& pkg)
			{
				lock_mutex();
				if(isOK() && isMaster())
				{
					i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();

					// Write register address and data to slave
					if(i2c_master_start(cmd_handle) != ESP_OK)
					{
						i2c_cmd_link_delete(cmd_handle);
						unlock_mutex();
						throw peripheral_error("Failed to start I2C master");
					}

					if(i2c_master_write_byte(cmd_handle, ( pkg.node_addr << 1 ) | I2C_MASTER_WRITE, true) != ESP_OK)
					{
						unlock_mutex();
						i2c_cmd_link_delete(cmd_handle);
						throw peripheral_error("Failed to register address byte");
					}

					if(i2c_master_write_byte(cmd_handle, pkg.reg_addr, true) != ESP_OK)
					{
						unlock_mutex();
						i2c_cmd_link_delete(cmd_handle);
						throw peripheral_error("Failed to register register address byte");
					}

					if(pkg.getTxBufferLen() > 0)
						if(i2c_master_write(cmd_handle, pkg.data(), pkg.getTxBufferLen(), true) != ESP_OK)
						{
							unlock_mutex();
							i2c_cmd_link_delete(cmd_handle);
							throw peripheral_error("Failed to register data bytes");
						}
					
					if(i2c_master_stop(cmd_handle) != ESP_OK)
					{
						unlock_mutex();
						i2c_cmd_link_delete(cmd_handle);
						throw peripheral_error("Failed to register stop byte");
					}

					_err = i2c_master_cmd_begin(PORT, cmd_handle, 1000 / portTICK_RATE_MS);
					if(_err != ESP_OK)
					{
						// printf("Committing write sequence failed:\n");
						i2c_cmd_link_delete(cmd_handle);
						unlock_mutex();
						switch(_err)
						{
							case ESP_ERR_INVALID_ARG: 	
								throw peripheral_error("Provided arguments are invalid");
							case ESP_FAIL: 				
								throw peripheral_error("Slave did not acknoledge");
							case ESP_ERR_INVALID_STATE: 
								throw peripheral_error("Driver is in invalid state");
							case ESP_ERR_TIMEOUT:	 	
								throw peripheral_error("Bus timed out");
						}
					}
					
					i2c_cmd_link_delete(cmd_handle);


					// Read data if expected
					if(pkg.mode == TRANSACTION::READ)
					{
						// Wait between write and read access
						vTaskDelay(pkg.wr_delay_ms / portTICK_RATE_MS);
						i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
						
						if(i2c_master_start(cmd_handle) != ESP_OK)
						{
							i2c_cmd_link_delete(cmd_handle);
							unlock_mutex();
							throw peripheral_error("Failed to start I2C master");
						}

						if(i2c_master_write_byte(cmd_handle, ( pkg.node_addr << 1 ) | I2C_MASTER_READ, true) != ESP_OK)
						{
							i2c_cmd_link_delete(cmd_handle);
							unlock_mutex();
							throw peripheral_error("Failed to register address byte");
						}
						
						pkg.insert(pkg.end(), pkg.getRxBufferLen() - pkg.size(), 0);
						if(i2c_master_read(cmd_handle, pkg.data(), pkg.getRxBufferLen(), I2C_MASTER_LAST_NACK) != ESP_OK)
						{
							i2c_cmd_link_delete(cmd_handle);
							unlock_mutex();
							throw peripheral_error("Failed to register read bytes");
						}

						if(i2c_master_stop(cmd_handle) != ESP_OK)
						{
							i2c_cmd_link_delete(cmd_handle);
							unlock_mutex();
							throw peripheral_error("Failed to register stop byte");
						}

						_err = i2c_master_cmd_begin(PORT, cmd_handle, 1000 / portTICK_RATE_MS);
						i2c_cmd_link_delete(cmd_handle);
						
						if(_err != ESP_OK)
						{
							// printf("Committing read sequence failed");
							unlock_mutex();
							switch(_err)
							{
								case ESP_ERR_INVALID_ARG: 	
		    						throw peripheral_error("Provided arguments are invalid");
								case ESP_FAIL: 				
		    						throw peripheral_error("Slave did not acknoledge");
								case ESP_ERR_INVALID_STATE: 
		    						throw peripheral_error("Driver is in invalid state");
								case ESP_ERR_TIMEOUT:	 	
		    						throw peripheral_error("Bus timed out");
							}
						}
					}
				}
				else
					throw peripheral_error("Bus is down");

				unlock_mutex();
			};

			inline void operator<<(i2c::packet& pkg) { push(pkg); };

			inline uint32_t getClockSpeed() const { return _clk_speed_Hz; }

			void discover()
			{
				i2c::packet cmd(0,1);
				cmd.node_addr 	= 0x00;
				cmd.reg_addr 	= 0x00;
				cmd.mode = peripheral::TRANSACTION::READ;
				
				printf("\n");
				printf("Discovering devices:\n");
				printf("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f:\n");
				
				if(!isOK())
					open_master(100000);

				for(auto it = 0; it < 8; ++it)
				{
					printf("%d0: ", it);
					for(auto it2 = 0; it2 < 16; ++it2)
					{
						try
						{
							push(cmd);
							printf(" %d%x", it, it2);
						}
						catch(...)
						{
							printf(" --");
							// printf("%s", e.what());
						}
						cmd.node_addr++;
					}
					printf("\n");
				}
			}

		private:
			inline bool isMaster() { return (_mode == I2C_MODE_MASTER); };
			inline constexpr bool isPort1() { return (PORT == I2C_NUM_0); };

			void lock_mutex() 
			{
				if(isPort1())
					xSemaphoreTake(i2c_port_mux[0], portMAX_DELAY);
				else
					xSemaphoreTake(i2c_port_mux[1], portMAX_DELAY);
			};

			void unlock_mutex()
			{
				if(isPort1())
					xSemaphoreGive(i2c_port_mux[0]);
				else
					xSemaphoreGive(i2c_port_mux[1]);
			};

			bool 		_isOK 				= false;
			esp_err_t 	_err 				= ESP_OK;
			i2c_mode_t 	_mode 				= I2C_MODE_MASTER;
			size_t 		_slv_rx_buf_len 	= 0; 
			size_t 		_slv_tx_buf_len 	= 0; 
			int 		_intr_alloc_flags 	= 0; 
			uint32_t 	_clk_speed_Hz 		= 100000;
			bool 		_slave_10bit_addr 	= false;
			uint16_t 	_slave_addr 		= 0;
		};

		// // ####################################################################################
		// // ####################################################################################

		template<i2c_port_t PORT, gpio_num_t SDA_PIN, gpio_num_t SCL_PIN>
		class bus_client
		{
		public:
			bus_client() : bus_client(UINT32_MAX) {};
			bus_client(uint32_t max_clock_freq_Hz) :  _max_clock_freq_Hz(max_clock_freq_Hz) {};

			void open(bool force = false)
			{
				if(!i2c_bus<PORT, SDA_PIN, SCL_PIN>::instance()->isOK() || 							// if the device was not opened yet (or closed meanwhile)
					(i2c_bus<PORT, SDA_PIN, SCL_PIN>::instance()->isOK() && (force == true)) || 		// if the device is up but the force parameter is set
					(i2c_bus<PORT, SDA_PIN, SCL_PIN>::instance()->getClockSpeed() > getMaxClockFrequency()))	// if the device has a lower max. clock freq. as currently set
					i2c_bus<PORT, SDA_PIN, SCL_PIN>::instance()->open_master(getMaxClockFrequency());
			};

			void push(i2c::packet& pkg) 
			{
				try
				{
					i2c_bus<PORT, SDA_PIN, SCL_PIN>::instance()->push(pkg); 
					_retry_count = 0;
				} 
				catch(peripheral_error)
				{
					_retry_count++;
					if(_retry_count < PUSH_RETRY_ATTEMPTS)
					{
						// i2c_bus<PORT, SDA_PIN, SCL_PIN>::instance()->clearBus(); 
						bus_client<PORT, SDA_PIN, SCL_PIN>::push(pkg);
					}
				}
			};

			void setMaxClockFrequency(uint32_t max_clock_freq_Hz) { _max_clock_freq_Hz = max_clock_freq_Hz; }
			inline uint32_t getMaxClockFrequency() const { return _max_clock_freq_Hz; }
			
		private:
			uint8_t _retry_count = 0;
			uint32_t _max_clock_freq_Hz;
		};
	}
}